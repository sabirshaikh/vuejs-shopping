// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import NProgress from 'nprogress'
import Vuex from 'vuex'
import store from './store/store'
import VueBlu from 'vue-blu'
import Vuelidate from 'vuelidate'
// import 'vue-blu/dist/css/vue-blu.min.css'
import '../node_modules/nprogress/nprogress.css';
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCszjGkqCi_K00SSdl8TJVuCbOZbYdeWBk",
    libraries: "places", // necessary for places input
  }
});

Vue.use(Vuelidate)

Vue.use(VueBlu)
// import 'vue-blu/dist/css/vue-blu.min.css'

// controls
// number control
import numberControl from './components/controls/number'
Vue.component('number-control', numberControl)

// alert message 
import alertBox from './components/controls/alert'
Vue.component('alert-box', alertBox)


Vue.use(Vuex);
NProgress.configure({ 
  showSpinner: false
});




Vue.config.productionTip = false
// emits methods
export const eventBus = new Vue({
  methods: {
    isOpen (value) {
      this.$emit('isOpen', value)
     
    },
    isClose (value) {
      this.$emit('isClose', value)
    },
    isShow (value) {
      this.$emit('isShow', value)
    },
    isHide (value) {
      this.$emit('isHide', value)
    },
    isModalShow (modal) {
      this.$emit('isModalShow', modal)
    },
    isModalClose () {
      this.$emit('isModalClose')
    },
    itemSelect(data, isShow) {
      this.$emit('itemSelect', data, isShow)
    }
  }
})
// currency
Vue.filter('currency', function (value) {
  return '$' + value;
})

// directive

// Vue.directive('color', {
//   bind (el, binding, vnode) {
//     console.log(binding.value)
    
//     if(binding.arg == 'bg') {
//       el.style.background =  binding.value
//     }
//     if(binding.arg == 'color') {
//       if(binding.modifiers['delay']) {
//         setTimeout(() => {
//           el.style.color =  binding.value
//         }, 2000);
//       }

     
//     }
    
//   },
//   update (el, binding) {
//     //console.log('update:', binding.value)
    
//   }
// })

/* eslint-disable no-new */
var vm = new Vue({ 
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})