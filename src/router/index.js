import Vue from 'vue'
import Router from 'vue-router'
import help from '@/components/pages/help'
import myaccount from '@/components/pages/my-account'
import storelocation from '@/components/pages/store-location'
import myCart from '@/components/pages/my-cart'
import processing from '@/components/pages/processing'
import orderComplete from '@/components/pages/order-completed'
import orderMenu from '@/components/pages/order-menu';
import errorPage from '@/components/pages/error';
import NProgress from 'nprogress'
Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Store Location',
      component: storelocation
    },
    {
      path: '/help',
      name: 'Help',
      component: help,
      props: { 
        pageTitle: true 
      }

    },
    {
      path: '/myaccount',
      name: 'my account',
      component: myaccount,
      meta: {
        isAuth: true
      }
    },
    { 
      path: '/order',
      name: 'OrderMenu',
      component: orderMenu,
    },
    {
      path: '/my-cart',
      name: 'my cart',
      component: myCart
    },
    {
      path: '/process',
      name: 'myCartProcessing',
      component: processing
    },
    {
      path: '/complete',
      name: 'myCartComplete',
      component: orderComplete
    },
    {
      path: '*',
      name: 'Error',
      component: errorPage
    }

  ],
  mode: 'history',
  linkActiveClass: "is-active",
  linkExactActiveClass: "exact-active",
  // linkActiveClass: 'is-active'
})

router.beforeResolve((to, from, next) => {
  if (to.name) {
      NProgress.start()
  }
  next()
})

router.afterEach((to, from) => {
  NProgress.done()
})


router.beforeEach((to, from, next) => {
  if(to.meta.isAuth) {
       return next('/')
  }
  next();
})

export default router