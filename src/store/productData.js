const productsData =  [
      {
        sku: 1,
        product: "sandwich 1",
        image: "static/img/sandwich1.png",
        images: [
          { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chimpanzee.jpg" },
          { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/gorilla.jpg" },
          { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/red-monkey.jpg" },
          { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mandrill-monkey.jpg" }
        ],
        description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
        details: "Descrpition of sandwich",
        price: 5.50,
        catId: 1
      },

      {
        sku: 2,
        product: "Sandwich 2",
        image: "static/img/sandwich2.jpg",
        description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
        details: "Descrpition of sandwich",
        price: 10,
        catId: 1
      },

      {
        sku: 3,
        product: "Sandwich 3",
        image: "static/img/sandwich3.jpg",
        description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
        details: "Descrpition of sandwich",
        price: 15,
        catId: 1
      },

      {
        sku: 4,
        product: "Sandwich 4",
        image: "static/img/sandwich4.jpg",
        description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
        details: "Descrpition of sandwich",
        price: 5,
        catId: 1
      },

      {
        sku: 5,
        product: "Sandwich 5",
        image: "static/img/sandwich5.jpg",
        description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
        details: "Descrpition of sandwich",
        price: 1,
        catId: 1
      },

      {
        sku: 6,
        product: "Sandwich 6",
        image: "static/img/sandwich6.jpg",
        description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
        details: "Descrpition of sandwich",
        price: 1.1,
        catId: 1
      },
        {
          sku: 7,
          product: "Bowl 1",
          image: "static/img/bowls1.jpg",
          images: [
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chimpanzee.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/gorilla.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/red-monkey.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mandrill-monkey.jpg" }
          ],
          description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
          details: "Descrpition of sandwich",
          price: 5.50,
          catId: 2
        },
        {
          sku: 8,
          product: "Bowl 2",
          image: "static/img/bowls2.jpg",
          images: [
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chimpanzee.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/gorilla.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/red-monkey.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mandrill-monkey.jpg" }
          ],
          description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
          details: "Descrpition of sandwich",
          price: 5.50,
          catId: 2
        },
        {
          sku: 9,
          product: "Bowl 3",
          image: "static/img/bowls3.jpg",
          images: [
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chimpanzee.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/gorilla.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/red-monkey.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mandrill-monkey.jpg" }
          ],
          description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
          details: "Descrpition of sandwich",
          price: 5.50,
          catId: 2
        },
        {
          sku: 10,
          product: "Bowl 4",
          image: "static/img/bowls4.jpg",
          images: [
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chimpanzee.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/gorilla.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/red-monkey.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mandrill-monkey.jpg" }
          ],
          description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
          details: "Descrpition of sandwich",
          price: 5.50,
          catId: 2
        },
        {
          sku: 11,
          product: "Bowl 5",
          image: "static/img/bowls5.jpg",
          images: [
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chimpanzee.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/gorilla.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/red-monkey.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mandrill-monkey.jpg" }
          ],
          description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
          details: "Descrpition of sandwich",
          price: 5.50,
          catId: 2
        },
        {
          sku: 12,
          product: "Bowl 6",
          image: "static/img/bowls6.jpg",
          images: [
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chimpanzee.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/gorilla.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/red-monkey.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mandrill-monkey.jpg" }
          ],
          description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
          details: "Descrpition of sandwich",
          price: 5.50,
          catId: 2
        },
        {
          sku: 13,
          product: "Bowl 7",
          image: "static/img/bowls1.jpg",
          images: [
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chimpanzee.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/gorilla.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/red-monkey.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mandrill-monkey.jpg" }
          ],
          description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
          details: "Descrpition of sandwich",
          price: 5.50,
          catId: 2
        },
        {
          sku: 15,
          product: "Bowl 8",
          image: "static/img/bowls4.jpg",
          images: [
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/chimpanzee.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/gorilla.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/red-monkey.jpg" },
            { image: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mandrill-monkey.jpg" }
          ],
          description: "Choose one of our fresh and famous salads or warm up with a bowl of hot and hearty soup. We’ve got new soup features daily.",
          details: "Descrpition of sandwich",
          price: 5.50,
          catId: 2
        }
];

const productCategory =  [
  {catId: 1, catName: 'Breakfast'},
  {catId: 2, catName: 'Bowls'}
]

export  {
  productsData,
  productCategory
} 