import Vue from 'vue'
import {productsData, productCategory} from './productData'
import {locationData} from './locations'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)
export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
    products: productsData,
    cartProducts: [],
    productCategory: productCategory,
    currentCat: null,
    locations: locationData,
    currentLocation: null
  },
  mutations: {
    setProducts (state) {
      state.products = productsData
    },
    setCartProduct (state, data) {
      var cartArray = state.cartProducts.find(items => items.sku === data.sku) 
      var indexArray = state.cartProducts.indexOf(cartArray)
      if (cartArray != null) {
        state.cartProducts[indexArray].quantity = data.quantity
        state.cartProducts[indexArray].sku = data.sku
      } else {
        state.cartProducts.push(data)
      }
    },
    updateCartProduct (state, data) {
      state.cartProducts[data.arrIndex] = data.proValue;
    },
    removeCartItem (state, index) {
      state.cartProducts.splice(index, 1);
    },
    setCategory (state, id) {
      state.currentCat = id
    },
    setLocation(state, id) {
      state.currentLocation = id
    }
  },
  actions: {
    fetchProducts ({commit}) {
      commit('setProducts')
    },
    addCartProduct ({commit}, data) {
      commit('setCartProduct', data)
    },
    updateCartProduct ({commit}, data) {
      commit('updateCartProduct', data)
    },
    removeCartItem ({commit}, data) {
      commit('removeCartItem', data)
    },
    setCategory ({commit}, id) {
      commit('setCategory', id)
    },
    setLocation({commit}, id) {
      commit('setLocation', id)
    }
  },
  getters: {
    getProducts (state) {
      return state.products
    },
    getCartproduct (state) {
      var i, j;
      var cartItems = [];
      for (i = 0; i < state.cartProducts.length; i++) {
         var cartArray = state.products.find(function(value, index) {
          if (value.sku === state.cartProducts[i].sku) {
              value.qua = state.cartProducts[i].quantity
             cartItems.push(value)
          }
        });
      }
      return cartItems;
    },
    getCartItems (state) {
      return state.cartProducts
    },
    getProductCategory (state) {
      return state.productCategory
    },
    getCurrentCategory (state) {
      return state.currentCat
    },
    getLocations(state) {
      return state.locations
    },
    getCurrentLocation(state) {
      return state.currentLocation
    }
  }
})
