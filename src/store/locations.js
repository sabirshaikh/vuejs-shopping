const locationData =  [
    {
        location_id: 1,
        location_name: 'Brampton - Queen St E',
        location_address: '2454 Queen Street E. Brampton ON L6S 5X9 Canada',
        distance: '35.7km',
        shop_status: 'Open Now',
        map_coordinates: [{lat: '43.738232'}, {long: '-79.703003'}],
        location_contact:[{pno: '+1 123 456 7890'}, {email: 'email@gmail.co.nz'}],
        open_hours: [
            {
                day: 'Monday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Tuesday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Wednesday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Thursday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Friday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Saturday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Sunday',
                time: '8:00am - 9:00pm'
            },

        ]
        
    },
    {
        location_id: 2,
        location_name: 'Zurich Life Building',
        location_address: '400 University Avenue (Simcoe Street between Queen and Dundas) Toronto ON M5G 1S6, Canada',
        distance: '5.7km',
        shop_status: 'Open Now',
        map_coordinates: [{lat: '43.653500'}, {long: '-79.388350'}],
        location_contact:[{pno: '+1 123 888 7890'}, {email: 'email@Zurich.co.nz'}],
        open_hours: [
            {
                day: 'Monday',
                time: '10:00am - 9:00pm'
            },
            {
                day: 'Tuesday',
                time: '10:00am - 9:00pm'
            },
            {
                day: 'Wednesday',
                time: '10:00am - 9:00pm'
            },
            {
                day: 'Thursday',
                time: '10:00am - 9:00pm'
            },
            {
                day: 'Friday',
                time: '10:00am - 9:00pm'
            },
            {
                day: 'Saturday',
                time: '10:00am - 9:00pm'
            },
            {
                day: 'Sunday',
                time: '10:00am - 9:00pm'
            },

        ]
        
    },
    {
        location_id: 3,
        location_name: 'Burlington - Brant St',
        location_address: '1250 Brant Street Burlington ON M4W 3B8 Canada',
        distance: '35.7km',
        shop_status: 'Open Now',
        map_coordinates: [{lat: '43.343070'}, {long: '-79.828560'}],
        location_contact:[{pno: '+1 456 123 7890'}, {email: 'email@Burlington.co.nz'}],
        open_hours: [
            {
                day: 'Monday',
                time: '8:00am - 7:00pm'
            },
            {
                day: 'Tuesday',
                time: '8:00am - 7:00pm'
            },
            {
                day: 'Wednesday',
                time: '8:00am - 7:00pm'
            },
            {
                day: 'Thursday',
                time: '8:00am - 7:00pm'
            },
            {
                day: 'Friday',
                time: '8:00am - 7:00pm'
            },
            {
                day: 'Saturday',
                time: '8:00am - 7:00pm'
            },
            {
                day: 'Sunday',
                time: '8:00am - 7:00pm'
            },

        ]
        
    },
    {
        location_id: 4,
        location_name: 'Hamilton - Discovery Dr',
        location_address: '47 Discovery Drive Hamilton ON M5T 2S8 Canada',
        distance: '37.7km',
        shop_status: 'Closed Now',
        map_coordinates: [{lat: '43.276460'}, {long: '-79.860820'}],
        location_contact:[{pno: '+1 123 456 7890'}, {email: 'email@gmail.co.nz'}],
        open_hours: [
            {
                day: 'Monday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Tuesday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Wednesday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Thursday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Friday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Saturday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Sunday',
                time: '8:00am - 9:00pm'
            },

        ]
        
    },
    {
        location_id: 5,
        location_name: 'Stoney Creek - Queenston Road',
        location_address: '140 Highway 8 Stoney Creek ON M2P2E5 Canada',
        distance: '43km',
        shop_status: 'Closed Now',
        map_coordinates: [{lat: '43.223190'}, {long: '-79.745150'}],
        location_contact:[{pno: '+1 123 456 7890'}, {email: 'email@gmail.co.nz'}],
        open_hours: [
            {
                day: 'Monday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Tuesday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Wednesday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Thursday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Friday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Saturday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Sunday',
                time: '8:00am - 9:00pm'
            },

        ]
        
    },
    {
        location_id: 6,
        location_name: 'McMaster University-Health Sciences Library',
        location_address: '1280 Main Street W. Hamilton ON M4N 3M5 Canada',
        distance: '53km',
        shop_status: 'Open Now',
        map_coordinates: [{lat: '43.257980'}, {long: '-79.917010'}],
        location_contact:[{pno: '+1 123 456 7890'}, {email: 'email@gmail.co.nz'}],
        open_hours: [
            {
                day: 'Monday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Tuesday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Wednesday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Thursday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Friday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Saturday',
                time: '8:00am - 9:00pm'
            },
            {
                day: 'Sunday',
                time: '8:00am - 9:00pm'
            },

        ]
        
    }
]

export  {
    locationData
} 